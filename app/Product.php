<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function photo()
    {
        return $this->hasMany(photo::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brands::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
