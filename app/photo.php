<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class photo extends Model
{

    protected  $uploads = '/storage/photos/';
    public function brands()
    {
        return $this->belongsTo(Brands::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getPathAttribute($photo)
    {
        return $this->uploads . $photo;
    }

}
