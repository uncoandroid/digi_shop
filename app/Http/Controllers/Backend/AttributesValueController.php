<?php

namespace App\Http\Controllers\Backend;

use App\AttributeGroup;
use App\AttributeValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributesValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributesvalue =AttributeValue::with('attributeGroup')->paginate(10);

        return view('admin.attributes-value.index',compact(['attributesvalue']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributesGroup = AttributeGroup::all();
        return view('admin.attributes-value.create',compact(['attributesGroup']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributesvalue = new AttributeValue();
        $attributesvalue->title = $request->input('attribute-title');
        $attributesvalue->attributeGroup_id = $request->input('attribute-group');
        $attributesvalue->save();
        return redirect('administrator/attributes-value');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attributesvalue = AttributeValue::with('attributeGroup')->where('id',$id)->first();
        $attributesGroup = AttributeGroup::all();
        return view('admin.attributes-value.edit',compact(['attributesvalue','attributesGroup']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributesvalue = AttributeValue::with('attributeGroup')->where('id',$id)->first();
        $attributesvalue->title = $request->input('attribute-title');
        $attributesvalue->attributeGroup_id = $request->input('attribute-group');
        $attributesvalue->save();
        return redirect('administrator/attributes-value');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attributesvalue = AttributeValue::findOrFail($id);
        $attributesvalue->delete();
        return redirect('administrator/attributes-value');
    }
}
