<?php

namespace App\Http\Controllers\Backend;

use App\Brands;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brands::with('photo')->paginate(10);
        return view('admin.brands.index', compact(['brands']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'title' => 'required|unique:brands',
                'description' => 'required',

            ],
            [
                'title.required' => 'عنوان برنج شما باید درج شود',
                'title.unique' => 'این برند قبلا ثبت شده است',
                'description.required' => ' توضیحات خود را وارد کنید'

            ]);
        if ($validator->fails()) {
            return redirect('administrator/brands')->withErrors($validator)->withInput();
        } else {
            $brand = new Brands();
            $brand->title = $request->input('title');
            $brand->description = $request->input('description');
            $brand->photo_id = $request->input('photo_id');
            $brand->save();
            return redirect('administrator/brands');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brands::with('photo')->where('id',$id)->first();
        return view('admin.brands.edit',compact(['brand']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(),
            [
                'title' => 'required|unique:brands,title,'.$id,
                'description' => 'required',

            ],
            [
                'title.required' => 'عنوان برنج شما باید درج شود',
                'title.unique' => 'این برند قبلا ثبت شده است',
                'description.required' => ' توضیحات خود را وارد کنید'

            ]);
        if ($validator->fails()) {
            return redirect('administrator/brands')->withErrors($validator)->withInput();
        } else {
            $brand = Brands::with('photo')->where('id',$id)->first();
            $brand->photo_id = $request->input('photo_id');
            $brand->title = $request->input('title');
            $brand->description = $request->input('description');
            $brand->save();
            return redirect('administrator/brands');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brands::with('photo')->where('id',$id)->first();
        $brand->delete();
        return redirect('administrator/brands');
    }
}
