<?php

namespace App\Http\Controllers\Backend;

use App\AttributeGroup;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AttributeGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attributesGroup = AttributeGroup::paginate(10);
        return view('admin.attributes.index',compact(['attributesGroup']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.attributes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributesGroup = new AttributeGroup();
        $attributesGroup->title = $request->input('attribute-title');
        $attributesGroup->type = $request->input('attribute-type');
        $attributesGroup->save();

        \Illuminate\Support\Facades\Session::flash('attribute_sucsses', 'ویژگی '.' ( '. $attributesGroup->title .' ) '.'با موفقیت به سیستم اضافه شد');
        return redirect('administrator/attributes-group');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attributesGroup = AttributeGroup::findOrFail($id);
        return view('admin.attributes.edit',compact(['attributesGroup']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributesGroup = AttributeGroup::findOrFail($id);
        $attributesGroup->title = $request->input('attribute-title');
        $attributesGroup->type = $request->input('attribute-type');
        $attributesGroup->save();

        return redirect('administrator/attributes-group');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attributesGroup = AttributeGroup::findOrFail($id);
        $attributesGroup->delete();
        return redirect('administrator/attributes-group');
    }
}
