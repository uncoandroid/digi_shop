<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    public function photo()
    {
        return $this->belongsTo(photo::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
