@extends('admin.layouts.master')

@section('dropzon_styles')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection

@section('main-content')

    <div class="box box-info colmd8" id="app">
        <div class="box-header with-border">
            <h2 class="box-title text-center">ایجاد برند جدید</h2>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">نام محصول :</label>
                            <input class="form-control" type="text" name="title" placeholder="نام محصول را وارد کنید">
                        </div>
                        <div class="form-group">
                            <label for="title">نام مستعار :</label>
                            <input class="form-control" type="text" name="slug" placeholder="نام مستعار را وارد کنید">
                        </div>
                        <div class="form-group">
                            <label for="status">وضعیت نشر :</label>

                            <div>
                                <input type="radio" name="status" value="0" checked><span>منتشر نشده</span>
                                <input type="radio" name="status" value="1"><span>منتشر شده</span>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="title">قیمت محصول :</label>
                            <input class="form-control" type="number" name="price"
                                   placeholder="قیمت محصول را وارد کنید">
                        </div>
                        <div class="form-group">
                            <label for="title">قیمت ویژه محصول :</label>
                            <input class="form-control" type="number" name="price"
                                   placeholder="قیمت ویژه محصول را وارد کنید">
                        </div>
                        <div class="form-group">
                            <label for="description">توضیحات محصول :</label>
                            <textarea class="form-control" name="description" id="" cols="30" rows="7"
                                      placeholder="توضیحات محصول را وارد کنید"></textarea>
                        </div>
                        <attribute-component :brands="{{$brands}}"></attribute-component>
                        <div class="form-group">
                            <label>آپلود عکس :</label>
                            <div class="form-group">
                                <input type="hidden" name="photo_id" id="brand_photo">
                                <div id="photo" class="dropzone dropzone_ui"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label>عنوان سئو :</label>
                            <input class="form-control" name="meta_title" type="text"
                                   placeholder="عنوان سئو را وارد کنید...">
                        </div>
                        <div class="form-group">
                            <label>توضیحات سئو :</label>
                            <textarea class="form-control" name="meta_desc" rows="7" type="text"
                                      placeholder="توضیحات سئو را وارد کنید..."></textarea>
                        </div>
                        <div class="form-group">
                            <label>کلمات کلیدی سئو :</label>
                            <input class="form-control" name="meta_keywords" type="text"
                                   placeholder="کلمات کلیدی سئو را وارد کنید...">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn  btn-success">ذخیره</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('script-vuejs')
    <script src="{{asset('admin/js/app.js')}}"></script>
@endsection
@section('dropzon_scripts')
    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        uploadMultiple: false;
        addRemoveLinks: true;
        dictRemoveFile: 'Remove file';
        dictFileTooBig: 'Image is larger than 16MB';
        timeout: 10000;

        var drop = new Dropzone("#photo", {
            url: "{{route('photos.upload')}}",
            sending: function (file, xhr, formData) {
                formData.append("_token", "{{csrf_token()}}")
            },
            success: function (file, response) {
                document.getElementById('brand_photo').value = response.photo_id
            }
        })
    </script>



@endsection