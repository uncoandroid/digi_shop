@extends('admin.layouts.master')


@section('main-content')

    <div class="col-md-12 main_content_ui">
        <div class="box box-info main_content_ui_shadow">
            <div class="box-header with-border">
                <h2 class="box-title text-center">محصولات</h2>

                <a class="btn btn-app pull-left" href="{{route('products.create')}}"><i class="fa fa-plus"></i> جدید</a>

            </div>


            @if(Session::has('error_category'))
                <div class="alert alert-danger">
                    <div>{{session('error_category')}}</div>
                </div>

        @endif

        <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th class="text-primary">شناسه</th>
                            <th class="text-primary">کد محصول</th>
                            <th class="text-primary">عنوان</th>
                            <th class="text-primary">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($products as $product)

                            <tr>
                                <td><span class="text">{{$product->id}}</span></td>
                                <td><span class="text">{{$product->sku}}</span></td>
                                <td><span class="text-bold">{{$product->title}}</span></td>
                                <div class="row">
                                    <td>
                                        <a class="btn btn-warning" href="{{route('products.edit',$product->id)}}">ویرایش</a>
                                        <div class="display_inline_block">
                                            <form method="post" action="{{url('administrator/products/'.$product->id)}}">
                                                {{@csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">حذف</button>
                                            </form>
                                        </div>
                                    </td>

                                </div>
                            </tr>



                        @endforeach

                        </tbody>
                    </table>
                    <div class="text-center ">
                        {{$products->links()}}
                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>

        </div>
    </div>

@endsection
