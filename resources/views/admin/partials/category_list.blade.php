@foreach($categories as $sub_category)
    <tr>
        <td><span class="text">{{$sub_category->id}}</span></td>
        <td><span class="text-danger">{{str_repeat('//ـــ' , $list_level)}} {{$sub_category->name}}</span></td>

        <div class="row">
            <td>
                <a class="btn btn-warning" href="{{route('categories.edit',$sub_category->id)}}">ویرایش</a>
                <div class="display_inline_block">
                    <form method="post" action="{{url('administrator/categories/'.$sub_category->id)}}">

                        {{@csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="btn btn-danger">حذف</button>
                        <a class="btn btn-primary" href="{{route('categories.indexSettings',$sub_category->id)}}">تنظیمات</a>
                    </form>
                </div>
            </td>

        </div>
    </tr>

    @if(count($sub_category->childrenRecursive) > 0)
        @include('admin.partials.category_list',['categories'=>$sub_category->childrenRecursive, 'list_level'=>$list_level+1])
    @endif
@endforeach
