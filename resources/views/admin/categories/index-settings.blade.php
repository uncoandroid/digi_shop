@extends('admin.layouts.master')


@section('main-content')

    <div class="box box-info colmd8">
        <div class="box-header with-border">
            <h2 class="box-title text-center">تعیین ویژگی دسته بندی <b class="text-bold text-primary"> ( {{$categories->name }} )</b></h2>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{route('categories.indexSettings',$categories->id)}}">
                        {{@csrf_field()}}

                        <div class="form-group">
                            <label for="attributeGroups">ویژگی های دسته بندی <b class="text-bold text-primary"> ( {{$categories->name }} )</b></label>
                            <select class="form-control" name="attributeGroups[]" multiple>
                                @foreach($attributeGroups as $attributeGroup)
                                    <option class="text-bold  text-primary" value="{{$attributeGroup->id}}" @if(in_array($attributeGroup->id ,  $categories->attributeGroups->pluck('id')->toArray())) selected @endif>{{$attributeGroup->title}}</option>
                                @endforeach
                            </select>
                            <div class=""><br>
                                <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                            </div>

                        </div>


                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
