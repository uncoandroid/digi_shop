@extends('admin.layouts.master')


@section('main-content')

    <div class="box box-info colmd8">
        <div class="box-header with-border">
            <h2 class="box-title text-center">ایجاد دسته بندی جدید</h2>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{url('administrator/categories')}}">
                        {{@csrf_field()}}
                        <div class="form-group">
                            <label for="name">نام دسته بندی :</label>
                            <input class="form-control" name="name" type="text"
                                   placeholder="عنوان دسته بندی را وارد کنید...">
                        </div>
                        <div class="form-group">
                            <label for="category_parent">دسته والد :</label>
                            <select class="form-control" name="category_parent" id="">
                                <option value="{{null}}">بدون والد</option>
                                @foreach($categories as $category)
                                    <option class="text-bold  text-primary" value="{{$category->id}}">{{$category->name}}</option>
                                    @if(count($category->childrenRecursive) > 0)
                                        @include('admin.partials.category',['categories'=>$category->childrenRecursive,'level'=>1])
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="meta_title">عنوان سئو :</label>
                            <input class="form-control" name="meta_title" type="text"
                                   placeholder="عنوان سئو را وارد کنید...">
                        </div>
                        <div class="form-group">
                            <label for="meta_desc">توضیحات سئو :</label>
                            <textarea class="form-control" name="meta_desc" rows="7" type="text"
                                      placeholder="توضیحات سئو را وارد کنید..."></textarea>
                        </div>
                        <div class="form-group">
                            <label for="meta_keywords">کلمات کلیدی سئو :</label>
                            <input class="form-control" name="meta_keywords" type="text"
                                   placeholder="کلمات کلیدی سئو را وارد کنید...">
                        </div>
                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>

                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
