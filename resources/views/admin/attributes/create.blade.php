@extends('admin.layouts.master')


@section('main-content')

    <div class="box box-info colmd8">
        <div class="box-header with-border">
            <h2 class="box-title text-center">ایجاد ویژگی جدید</h2>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{url('administrator/attributes-group')}}">
                        {{@csrf_field()}}
                        <div class="form-group">
                            <label for="attribute-title">عنوان ویژگی :</label>
                            <input class="form-control" name="attribute-title" type="text"
                                   placeholder="عنوان گروه ویژگی را وارد کنید...">
                        </div>

                        <div class="form-group">
                            <label for="attribute-type">نوع ویژگی :</label>
                            <select class="form-control" name="attribute-type" id="">
                                <option value="select">لیست تکی</option>
                                <option value="multiple">لیست چندتایی</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>

                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
