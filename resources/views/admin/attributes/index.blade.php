@extends('admin.layouts.master')


@section('main-content')

    <div class="col-md-12 main_content_ui">
        <div class="box box-info main_content_ui_shadow">
            <div class="box-header with-border">
                <h2 class="box-title text-center">ویژگی ها</h2>

                <a class="btn btn-app pull-left" href="{{route('attributes-group.create')}}"><i class="fa fa-plus"></i>
                    جدید</a>

            </div>
            @if (Session::has('attribute_sucsses'))
                <div class="alert alert-success">
                    <div>{{session('attribute_sucsses')}}</div>
                </div>


        @endif

        <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th class="text-primary">شناسه</th>
                            <th class="text-primary">عنوان</th>
                            <th class="text-primary">نوع</th>
                            <th class="text-primary">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($attributesGroup as $attribute)
                            <tr>
                                <td>{{$attribute->id}}</td>
                                <td>{{$attribute->title}}</td>
                                <td>{{$attribute->type}}</td>
                                <div class="row">
                                    <td>
                                        <a class="btn btn-warning"
                                           href="{{route('attributes-group.edit',$attribute->id)}}">ویرایش</a>
                                        <div class="display_inline_block">
                                            <form method="post"
                                                  action="{{route('attributes-group.destroy',$attribute->id)}}">
                                                {{@csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">حذف</button>
                                            </form>
                                        </div>
                                    </td>

                                </div>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <div class="text-center ">
                        {{$attributesGroup->links()}}
                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>

        </div>
    </div>

@endsection
