@extends('admin.layouts.master')


@section('main-content')

    <div class="col-md-12 main_content_ui">
        <div class="box box-info main_content_ui_shadow">
            <div class="box-header with-border">
                <h2 class="box-title text-center">لیست برند ها</h2>

                <a class="btn btn-app pull-left" href="{{route('brands.create')}}"><i class="fa fa-plus"></i> جدید</a>

            </div>
            @include('admin.partials.form-errors')

            @if(Session::has('error_category'))
                <div class="alert alert-danger">
                    <div>{{session('error_category')}}</div>
                </div>

        @endif

        <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th class="text-primary">شناسه</th>
                            <th class="text-primary">عنوان</th>
                            <th class="text-primary">تصویر برند</th>
                            <th class="text-primary">عملیات</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($brands as $brand)

                            <tr>
                                <td><span class="text">{{$brand->id}}</span></td>
                                <td><span class="text-bold">{{$brand->title}}</span></td>
                                <td><img src="{{asset($brand->photo->path)}}" alt="" class="img-fluid" width="80"></td>

                                <div class="row">
                                    <td>
                                        <a class="btn btn-warning" href="{{route('brands.edit',$brand->id)}}">ویرایش</a>
                                        <div class="display_inline_block">
                                            <form method="post" action="{{url('administrator/brands/'.$brand->id)}}">
                                                {{@csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger">حذف</button>
                                            </form>
                                        </div>
                                    </td>

                                </div>
                            </tr>



                        @endforeach

                        </tbody>
                    </table>

                </div>
                <!-- /.table-responsive -->
            </div>

        </div>
    </div>

@endsection
