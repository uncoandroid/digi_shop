@extends('admin.layouts.master')

@section('dropzon_styles')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection

@section('main-content')

    <div class="box box-info colmd8">
        <div class="box-header with-border">
            <h2 class="box-title text-center">ویرایش برند <b class="text-bold text-primary"> ( {{ $brand->title }} )</b></h2>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{url('administrator/brands/'.$brand->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                            <label for="description">تصویر برند :</label>
                            <div class="text-center">
                                <img src="{{asset($brand->photo->path)}}" class="img-fluid" width="280">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title">نام برند :</label>
                            <input class="form-control" type="text" name="title" value="{{$brand->title}}">
                        </div>
                        <div class="form-group">
                            <label for="description">توضیحات برند :</label>
                            <textarea class="form-control" name="description" id="" cols="30" rows="7">{{$brand->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="upload">آپلود عکس :</label>
                            <div class="form-group">
                                <input type="hidden" name="photo_id" id="brand_photo" value="{{$brand->photo_id}}">
                                <div id="photo" class="dropzone dropzone_ui"></div>
                            </div>

                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn  btn-success">ذخیره</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('dropzon_scripts')
    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        uploadMultiple: false;
        addRemoveLinks: true;
        dictRemoveFile: 'Remove file';
        dictFileTooBig: 'Image is larger than 16MB';
        timeout: 10000;

        var drop = new Dropzone("#photo", {
            url: "{{route('photos.upload')}}",
            sending: function (file, xhr, formData) {
                formData.append("_token", "{{csrf_token()}}")
            },
            success: function (file, response) {
                document.getElementById('brand_photo').value = response.photo_id
            }
        })

    </script>
@endsection