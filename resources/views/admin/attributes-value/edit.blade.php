@extends('admin.layouts.master')


@section('main-content')

    <div class="box box-info colmd8">
        <div class="box-header with-border">
            <h2 class="box-title text-center">ویرایش ویژگی <b class="text-primary"> ( {{$attributesvalue->title}} )</b>
            </h2>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{url('administrator/attributes-value/'.$attributesvalue->id)}}">
                        {{@csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                            <label for="attribute-title">عنوان ویژگی :</label>
                            <input class="form-control" name="attribute-title" type="text"
                                   value="{{$attributesvalue->title}}" placeholder="عنوان گروه ویژگی را وارد کنید...">
                        </div>

                        <div class="form-group">
                            <label for="attribute-type">نوع ویژگی :</label>
                            <select class="form-control" name="attribute-group" id="">
                                @foreach ($attributesGroup as $attributeGr)
                                    <option value="{{$attributeGr->id}}" @if($attributesvalue->attributeGroup_id == $attributeGr->id)  selected @endif>{{$attributeGr->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
