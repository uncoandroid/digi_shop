@extends('admin.layouts.master')


@section('main-content')

    <div class="box box-info colmd8">
        <div class="box-header with-border">
            <h2 class="box-title text-center">ایجاد مقادیر ویژگی جدید</h2>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <form method="post" action="{{url('administrator/attributes-value')}}">
                        {{@csrf_field()}}

                        <div class="form-group">
                            <label for="">انتخاب ویژگی :</label>
                            <select class="form-control" name="attribute-group" id="">
                                <option  value="{{null}}">بدون ویژگی</option>
                                @foreach ($attributesGroup as $attribute)
                                    <option value="{{$attribute->id}}">{{$attribute->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="attribute-title">عنوان مقادیر ویژگی :</label>
                            <input class="form-control" name="attribute-title" type="text" placeholder="عنوان مقادیر ویژگی را وارد کنید...">
                        </div>





                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>

                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
