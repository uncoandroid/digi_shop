<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('api')->group(function () {
    Route::get('/categories', 'Backend\CategoryController@apiIndex');
    Route::post('/categories/attribute', 'Backend\CategoryController@apiIndexAttribute');
});


Route::prefix('administrator')->group(function () {
    Route::get('/', 'Backend\MainController@masterPage');
    Route::resource('categories', 'Backend\CategoryController');
    Route::get('categories/{id}/settings','Backend\CategoryController@indexSetting')->name('categories.indexSettings');
    Route::post('categories/{id}/settings','Backend\CategoryController@saveSetting');
    Route::resource('attributes-group', 'Backend\AttributeGroupController');
    Route::resource('attributes-value', 'Backend\AttributesValueController');
    Route::resource('brands','Backend\BrandsController');
    Route::resource('photos','Backend\PhotosController');
    Route::post('photos/upload','Backend\PhotosController@upload')->name('photos.upload');
    Route::resource('products','Backend\ProductController');

});


